import { assertAbortSignal, isAbortError } from "abort-tools";
import assert from "assert";
import { BufferedIterable, createBufferedIterable } from "buffered-iterable";
import { second } from "msecs";
import test from "tape-promise/tape.js";
import { createQueryMemoizer } from "./query-memoizer.js";
import { waitFor } from "./wait.js";

test("reducer errors", async t => {
    let lastError: unknown;

    let bufferedIterable!: BufferedIterable<boolean>;
    const queryMemoizer = createQueryMemoizer({
        initialState: {
            count: 0,
        },
        reduce: (state, event) => {
            let { count } = state;
            count += 1;
            assert(count <= 1);
            return {
                ...state,
                count,
            };
        },
        source: signal => {
            bufferedIterable = createBufferedIterable();
            signal.addEventListener(
                "abort",
                () => bufferedIterable!.done(),
                { once: true },
            );
            bufferedIterable.push(true);
            return bufferedIterable;
        },
        retryIntervalBase: 1 * second,
        retryIntervalCap: 1 * second,
        onError(error) {
            lastError = error;
        },
    });

    const queryPromise = queryMemoizer.acquire();
    try {
        const query = await queryPromise;

        bufferedIterable.push(true);

        await waitFor(
            () => assert(lastError != null),
        );

        t.pass();
    }
    finally {
        queryMemoizer.release(queryPromise);
    }

});

test("event errors", async t => {
    let lastError: unknown;

    const retryIntervalBase = 1 * second;
    const retryIntervalCap = 1 * second;

    const queryMemoizer = createQueryMemoizer({
        initialState: {
            count: 0,
        },
        reduce(state, event) {
            let { count } = state;
            if (event) {
                count++;
            }
            else {
                count--;
            }
            return { count };
        },
        async * source(signal) {
            while (!signal.aborted) {
                yield true;

                assertAbortSignal(signal, "aborted");

                throw new Error("error");
            }

        },

        retryIntervalBase,
        retryIntervalCap,

        onError(error) {
            lastError = error;
        },
    });

    const queryPromise = queryMemoizer.acquire();
    try {
        const query = await queryPromise;

        {
            const state = query.getState();
            t.deepEqual(
                state,
                { count: 1 },
            );
        }

        const abortController = new AbortController();

        const iterable = query.fork(abortController.signal);
        const iterator = iterable[Symbol.asyncIterator]();

        try {
            {
                const next = await iterator.next();
                assert(next.done !== true);
                t.deepEqual(
                    next.value,
                    {
                        state: { count: 2 },
                        event: true,
                    },
                );
            }

            {
                const next = await iterator.next();
                assert(next.done !== true);
                t.deepEqual(
                    next.value,
                    {
                        state: { count: 3 },
                        event: true,
                    },
                );
            }
        }
        finally {
            abortController.abort();

            await iterator.next().then(
                () => t.fail("expected abort error"),
                error => t.ok(isAbortError(error), "expected abort error"),
            );
        }

        await waitFor(
            () => assert(lastError != null),
        );
    }
    finally {
        queryMemoizer.release(queryPromise);
    }

});

test("should not panick in case of a bad event", async t => {
    let lastError: unknown;

    const retryIntervalBase = 1 * second;
    const retryIntervalCap = 1 * second;

    let reduceAttempts = 0;
    let bufferedIterable!: BufferedIterable<number>;

    const queryMemoizer = createQueryMemoizer({
        initialState: {
            count: 0,
        },
        reduce: (state, event) => {
            reduceAttempts++;
            if (reduceAttempts % 2 === 0) {
                throw new Error();
            }

            return {
                ...state,
                count: state.count + 1,
            };
        },
        source: signal => {
            bufferedIterable = createBufferedIterable();

            for (let i = 0; i < 5; i++) {
                bufferedIterable.push(i);
            }

            // and return the stream with the single event
            return bufferedIterable;
        },

        retryIntervalBase,
        retryIntervalCap,

        onError(error) {
            lastError = error;
        },
    });

    const controller = new AbortController();

    const queryPromise = queryMemoizer.acquire();
    try {
        const query = await queryPromise;

        const iterable = query.fork(controller.signal);
        const itr = iterable[Symbol.asyncIterator]();

        for (let i = 1; i <= 5; i++) {
            await itr.next();
        }

        t.pass();

        await waitFor(
            () => assert(lastError != null),
        );
    }
    finally {
        queryMemoizer.release(queryPromise);
    }

});
