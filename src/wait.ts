import delay from "delay";

export async function waitFor<T>(
    task: () => Promise<T> | T,
    interval = 200,
    cycles = 20,
): Promise<T> {
    for (let cycle = 0; cycle < cycles; cycle++) {
        try {
            const value = await task();
            return value;
        }
        catch {
            await delay(interval);
        }
    }

    const value = await task();
    return value;
}
